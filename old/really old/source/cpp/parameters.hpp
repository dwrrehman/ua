//
//  parameters.hpp
//  ua
//
//  Created by Daniel Rehman on 1910137.
//                                                       
//

#ifndef parameters_hpp
#define parameters_hpp

#include "utilities.hpp"


void debug_parameters(parameters u);
void print_usage();
parameters compute_parameters(const char** argv, const int argc);

#endif /* parameters_hpp */
