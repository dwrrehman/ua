//
//  visualizer.hpp
//  ua
//
//  Created by Daniel Rehman on 1910137.
//

#ifndef visualizer_hpp
#define visualizer_hpp

#include "utilities.hpp"
#include "parameters.hpp"

#include "io.hpp"
#include "ca_util.hpp"
#include "symbolic.hpp"

#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <vector>
#include <thread>

void visualize(const parameters& u);
    
#endif /* visualizer_hpp */
